import 'package:flutter/material.dart';
import 'auth.dart';
import 'root.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {

@override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Logistic App',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: new RootPage(auth: new Auth())
    );
  }
}


import 'package:firebase_auth/firebase_auth.dart';
import 'dart:async';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';


abstract class BaseAuth {
  Future<String> createUserWithEmailAndPassword(String email, String password);
  Future<String> signInWithEmailAndPassword(String email, String password);
  Future<FirebaseUser> signInWithGmail();
  Future<FirebaseUser> signInWithFacebook();
  Future<String> currentUser();
  Future<void> signOut();
}

class Auth implements BaseAuth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = new GoogleSignIn();
  final FacebookLogin _facebookLogin = new FacebookLogin();

  Future<String> signInWithEmailAndPassword(String email, String password) async{
    FirebaseUser user = await _firebaseAuth.signInWithEmailAndPassword(email: email,password: password);
    return user.uid;
  }

  Future<String> createUserWithEmailAndPassword(String email, String password) async {
    FirebaseUser user = await _firebaseAuth.createUserWithEmailAndPassword(email: email, password: password);
    return user.uid;
  }

  Future<FirebaseUser> signInWithGmail() async {
    GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
    GoogleSignInAuthentication gSa = await googleSignInAccount.authentication;
    FirebaseUser user = await _firebaseAuth.signInWithGoogle(
      idToken: gSa.idToken,
      accessToken: gSa.accessToken
    );
    return user;
  }

  Future<FirebaseUser> signInWithFacebook() async {
    FacebookLoginResult result = await _facebookLogin.logInWithReadPermissions(['email','public_profile']);
    FirebaseUser user = await _firebaseAuth.signInWithFacebook(accessToken: result.accessToken.token);
    return user;
  }

  Future<String> currentUser() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    return user.uid;
  }

  Future<void> signOut() async{
    return _firebaseAuth.signOut();
  }

}
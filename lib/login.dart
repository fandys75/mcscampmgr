import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'auth.dart';

class LoginPage extends StatefulWidget{
  LoginPage({this.auth, this.onSignedIn});
  final BaseAuth auth;
  final VoidCallback onSignedIn;

  @override
    State<StatefulWidget> createState() => new _LoginPageState();

    }

    enum FormType{
      login,
      register
    }

    class _LoginPageState extends State<LoginPage> {

        final formKey = new GlobalKey<FormState>();
      
        String _email;
        String _password;
        FormType _formType = FormType.login;

    bool validateAndSave(){
      final form = formKey.currentState;
      if(form.validate()){
        form.save();
        return true;
      } 
      return false;
    }

    //Button Submit Proses
    void validateAndSubmit() async{
      if(validateAndSave()){
        try{
          if(_formType == FormType.login){
            String userId = await widget.auth.signInWithEmailAndPassword(_email, _password);
            print('Signed in : $userId');
          } else {
            String userId = await widget.auth.createUserWithEmailAndPassword(_email, _password);
            print('Registered User : $userId');
          }
          widget.onSignedIn();
        }
        catch(e) {
          print('Error: $e');
        }
      }
    }

    void goToRegister(){
      formKey.currentState.reset();
      setState(() {
              _formType = FormType.register;
            });
    }

    void goToLogin(){
      formKey.currentState.reset();
      setState(() {
            _formType = FormType.login;
          });     
    }

    void goSigninGmail() async{
      try {
        FirebaseUser userId = await widget.auth.signInWithGmail();
        print('Signed in : $userId');
      } catch (e) {
        print('Error: $e');
      }
    }

    void goSigninFacebook() async{
      try {
        FirebaseUser userId = await widget.auth.signInWithFacebook();
        print('Signed in : $userId');
      } catch (e) {
        print('Error: $e');
      }
    }

  @override
    Widget build(BuildContext context) {
      return new Scaffold(
        appBar: new AppBar(
          title: new Text('Login'),
        ),
        body: new Container(
          padding: EdgeInsets.all(20.0),
          child: new Form(
            key: formKey,
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: buildInput() + buildSubmitButtons(),
            ),
          )
        )
      );
    }

    List<Widget> buildInput(){
      return[
        new TextFormField(
          decoration: new InputDecoration(labelText: 'Email'),
          validator: (value) => value.isEmpty ? 'Email Tidak Boleh Kosong' : null,
          onSaved: (value) => _email = value,
          ),
        new TextFormField(
          decoration: new InputDecoration(labelText: 'Password'),
          obscureText: true,
          validator: (value) => value.isEmpty ? 'Password Tidak Boleh Kosong' : null,
          onSaved: (value) => _password = value,
          ),
      ];
    }

    List<Widget> buildSubmitButtons(){
      if(_formType == FormType.login){
        return [
          new RaisedButton(
            child: new Text('Login', style: new TextStyle(fontSize: 20.0)),
            onPressed: validateAndSubmit,
            ),
          new RaisedButton(
            child: new Text('Signin Via Gmail', style: new TextStyle(fontSize: 20.0)),
            onPressed: goSigninGmail,
            ),
          new RaisedButton(
            child: new Text('Signin Via Facebook', style: new TextStyle(fontSize: 20.0)),
            onPressed: goSigninFacebook,
            ),
          new FlatButton(
            child: new Text('Registrasi', style: new TextStyle(fontSize: 20.0)),
            onPressed: goToRegister,
            )
        ];
      } else{
          return [
          new RaisedButton(
            child: new Text('Daftar', style: new TextStyle(fontSize: 20.0)),
            onPressed: validateAndSubmit,
            ),
          new FlatButton(
            child: new Text('Sudah Punya Akun? Silahkan Login', style: new TextStyle(fontSize: 20.0)),
            onPressed: goToLogin,
            )
        ];
      }
    }
}